#include <QCoreApplication>
#include "alt.h"
#include <iostream>
#include <QtDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QTextStream(stdout) << "Alternating caps method! \n";
    QString test = "this is a test message";
    qDebug("The input message is: %s", qPrintable(test));

    alt(test);
    QTextStream(stdout) << "The output is: " << alt(test);
    //QString return_message = alt(test);

    //const auto test_return = QString{return_message};








    return a.exec();
}
