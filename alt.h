#ifndef ALT_H
#define ALT_H
#pragma once
#include <string>
#include <QCoreApplication>

QString alt(QString input);
QChar setCase(QChar letter, bool upper);
#endif // ALT_H
